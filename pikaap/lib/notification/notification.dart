import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'toolsnotification.dart';
import 'package:pikaap/toolshome.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationState createState() => _NotificationState();
}

class _NotificationState extends State<Notifications> {
  List<bool> _isExpanded = List.generate(1, (_) => false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: color1,
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Image.asset('images/filter.png'),
                    ),
                    IconButton(
                        splashColor: Colors.blue[100],
                        onPressed: () {
                          setState(() {
                            Get.back();
                          });
                        },
                        icon: Icon(Icons.keyboard_arrow_right)),
                  ],
                ),
              ),
              Expanded(
                  child: Container(
                child: ListView(children: [
                  Container(
                    margin: EdgeInsets.all(10),
                    decoration: decorationcadr(),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              time.year.toString() +
                                  "." +
                                  time.month.toString() +
                                  "." +
                                  time.day.toString(),
                              style: styledate,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              time.hour.toString() +
                                  ":" +
                                  time.minute.toString(),
                              style: styledate,
                            ),
                          ],
                        ),
                        Text(discription[0]),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    decoration: decorationcadr(),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              time.year.toString() +
                                  "." +
                                  time.month.toString() +
                                  "." +
                                  time.day.toString(),
                              style: styledate,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              time.hour.toString() +
                                  ":" +
                                  time.minute.toString(),
                              style: styledate,
                            ),
                          ],
                        ),
                        Text(discription[1]),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    decoration: decorationcadr(),
                    padding: EdgeInsets.all(10),
                    child: ExpansionPanelList(
                      animationDuration: Duration(seconds: 1),
                      expansionCallback: (index, isExpanded) => setState(() {
                        _isExpanded[index] = !isExpanded;
                      }),
                      children: [
                        for (int i = 0; i < 1; i++)
                          ExpansionPanel(
                            body: Container(
                              margin: EdgeInsets.all(10),
                              decoration: decorationcadr(),
                              //padding: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  Text(discription[3]),
                                ],
                              ),
                            ),
                            headerBuilder: (_, isExpanded) {
                              return Container(
                                /// margin: EdgeInsets.all(10),
                                decoration: decorationcadr(),
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          time.year.toString() +
                                              "." +
                                              time.month.toString() +
                                              "." +
                                              time.day.toString(),
                                          style: styledate,
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          time.hour.toString() +
                                              ":" +
                                              time.minute.toString(),
                                          style: styledate,
                                        ),
                                      ],
                                    ),
                                    Text(discription[2]),
                                  ],
                                ),
                              );
                            },
                            isExpanded: _isExpanded[i],
                          )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    decoration: decorationcadr(),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              time.year.toString() +
                                  "." +
                                  time.month.toString() +
                                  "." +
                                  time.day.toString(),
                              style: styledate,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              time.hour.toString() +
                                  ":" +
                                  time.minute.toString(),
                              style: styledate,
                            ),
                          ],
                        ),
                        Text(discription[4]),
                      ],
                    ),
                  ),
                ]),
              )),
            ],
          ),
        ),
      ),
    );
  }
}
