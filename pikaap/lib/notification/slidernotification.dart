import 'package:flutter/material.dart';
import 'notification.dart';

class SliderNotification extends PageRouteBuilder {
  final Widget widget;

  SliderNotification(this.widget)
      : super(
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return SlideTransition(
                position: Tween(
                  begin: Offset(-0.5, -0.5),
                  end: Offset.zero,
                ).animate(animation),
                child: child,
              );
            },
            transitionDuration: Duration(seconds: 1),
            pageBuilder: (context, animation, secondaryAnimation) {
              return Notifications();
            });
}
