import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:pikaap/toolshome.dart';
import 'package:pikaap/wallet/toolswallet.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class Wallet extends StatefulWidget {
  @override
  _WallteState createState() => _WallteState();
}

class _WallteState extends State<Wallet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Center(
            child: Column(
              children: [
                actionwallet(),
                SizedBox(height: 100),
                labelitems(),
                listTransactions()
              ],
            ),
          ),
        ),
      ),
    );
  }

  actionwallet() {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: 150,
          color: color1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundColor: color2,
                    child: Image.asset('images/home/wallet.png'),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    color: Colors.white,
                    child: Text(
                      'اعتبار فعلی شما: 490.000 ریال',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
              IconButton(
                  onPressed: () {
                    setState(() {
                      Get.back();
                    });
                  },
                  icon: Icon(Icons.keyboard_arrow_right)),
            ],
          ),
        ),
        Positioned(
            top: 110,
            right: 20,
            left: 20,
            child: Container(
              decoration: inputwallet(),
              width: 300,
              height: 80,
              child: inputmonywallet(),
            )),
        Positioned(
            top: 200,
            right: 20,
            left: 20,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      backgroundColor: color2,
                      child: Image.asset('images/filter.png'),
                    ),
                    Text('تاریخچه تراکنش ها'),
                  ],
                ),
                DottedLine(
                  dashColor: color1,
                ),
              ],
            )),
        // Positioned(
        //     top: 280,
        //     right: 20,
        //     left: 20,
        //     child: listTransactions()),
      ],
      overflow: Overflow.visible,
    );
  }

  inputmonywallet() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
          splashColor: Colors.red,
          onTap: () {
            setState(() {
              String get = input.text.toString();
              Get.snackbar(get, 'این مبلغ شارژ شد. ',
                  icon: Icon(
                    Icons.wallet_travel,
                    color: Colors.green,
                  ));
            });
          },
          child: Container(
            width: 90,
            height: 50,
            decoration: payment(),
            child: Center(
              child: Text(
                'پرداخت',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.50,
          height: 50,
          child: TextField(
            controller: input,
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.right,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              filled: true,
              hintText: 'مبلغ شارژ به ریال',
              fillColor: color2,
              border: OutlineInputBorder(
                  //borderSide: BorderSide(width: 0, color: color2),
                  borderRadius: BorderRadius.circular(10)),
            ),
          ),
        )
      ],
    );
  }

  listTransactions() {
    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      child: Container(
        height: 300,
        margin: EdgeInsets.all(15),
        width: double.infinity,
        decoration: inputwallet(),
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, int indexs) {
              return Container(
                height: 50,
                decoration: decoratonlisttaraconesh(),
                margin: EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 5,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('250.000'),
                    Text('پیکاپ'),
                    Text('12:23'),
                    Text('00/05/05'), //datetaraconesh
                    Text('123'), //codetaraconesh
                  ],
                ),
              );
            }),
      ),
    );
  }

  labelitems() {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            'مبلغ',
            style: stylelabel,
          ),
          Text(
            'نوع',
            style: stylelabel,
          ),
          Text(
            'ساعت',
            style: stylelabel,
          ),
          Text(
            'تاریخ',
            style: stylelabel,
          ),
          Text(
            'کد تراکنش',
            style: stylelabel,
          ),
        ],
      ),
    );
  }
}
