import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pikaap/notification/notification.dart';
import 'package:pikaap/notification/slidernotification.dart';
import 'package:pikaap/origin_destination/origin.dart';
import 'package:pikaap/wallet/wallet.dart';
import 'menu/menu.dart';
import 'menu/slidermenu.dart';
import 'toolshome.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: EdgeInsets.all(5),
          child: Center(
            child: Column(
              children: [
                items1(),
                SizedBox(
                  height: 130,
                ),
                itemsgrid(),

                //panelTabligh(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  items1() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: 160,
            color: color1,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        //Get.to(Notifications());
                        Navigator.push(context, SliderNotification(widget));
                      });
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Image.asset(
                        'images/home/alarm.png',
                        width: 25,
                        height: 25,
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        radius: 30.0,
                        backgroundColor: Colors.white,
                        child: Image.asset(
                          'images/home/person.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        width: 110,
                        height: 25,
                        decoration: decrotioncardtitle(),
                        child: Text(
                          'شاهین مرامی',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        // Get.to(Menu());

                        Navigator.push(context, SliderMenu(widget));
                      });
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Image.asset(
                        'images/home/layer.png',
                        width: 25,
                        height: 25,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 130,
            left: 15,
            right: 15,
            child: Container(
              width: 400,
              height: 60,
              decoration: decrotionitem1(),
              child: Item1_1(),
            ),
          ),
          Positioned(
            top: 200,
            left: 15,
            right: 15,
            child: panelTabligh(),
          ),
        ],
        overflow: Overflow.visible,
      ),
    );
  }

  Item1_1() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RaisedButton(
            color: color1,
            splashColor: Colors.blue,
            onPressed: () {
              setState(() {
                // Get.snackbar('اعتبار', 'اعتبار شما افزایش یافت.',
                //     icon: Icon(
                //       Icons.add_alert_outlined,
                //       color: Colors.red,
                //     ));
                Get.to(Wallet());
              });
            },
            child: Text('افزایش اعتبار'),
          ),
          Row(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('اعتبار شما'),
                  Text('490.000' + ' ریال'),
                ],
              ),
              SizedBox(
                width: 2,
              ),
              CircleAvatar(
                radius: 25,
                backgroundColor: Color(0xffF4F7FF),
                child: Image.asset('images/home/wallet.png'),
              )
            ],
          )
        ],
      ),
    );
  }

  panelTabligh() {
    return Container(
      //margin: EdgeInsets.all(15),
      width: double.infinity,
      height: 100,
      decoration: decrotiontabligh(),
      child: Center(
        child: Text(
          strtabligh,
          style: styletabligh,
        ),
      ),
    );
  }

  itemsgrid() {
    return Container(
      margin: EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width * 0.85,
      height: 350,
      child: GridView.builder(
          scrollDirection: Axis.vertical,
          itemCount: nameitem.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 15,
            crossAxisSpacing: 15,
          ),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Origin();
                  }));
                });
              },
              child: Container(
                  width: 150,
                  height: 150,
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(colors: [
                        begincolor[index],
                        endcolor[index],
                      ])),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      imggride[index],
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          FlatButton(
                            splashColor: begincolor[index],
                            height: 20,
                            minWidth: 20,
                            color: Colors.white.withAlpha(100),
                            onPressed: () {
                              Get.to(Origin());
                            },
                            child: Icon(
                              Icons.arrow_back,
                              size: 20,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            nameitem[index],
                            style: styletxtgrid,
                          ),
                        ],
                      )
                    ],
                  )),
            );
          }),
    );
  }
}
