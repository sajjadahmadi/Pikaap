import 'package:flutter/material.dart';
import 'package:pikaap/home.dart';
import 'package:pikaap/origin_destination/toolsorigin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'destination.dart';

class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/maps.png'), fit: BoxFit.cover)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Home();
                        }));
                      });
                    },
                    icon: Icon(Icons.keyboard_arrow_right))
              ],
            ),
          ),
          bottomsheet(),
        ],
      ),
    );
  }

  bottomsheet() {
    return DraggableScrollableSheet(
      initialChildSize: 0.1,
      minChildSize: 0.1,
      maxChildSize: 0.6,
      builder: (context, controller) {
        return Container(
          height: 50,
          decoration: decbottomsheet(),
          child: ListView(
            controller: controller,
            children: [
              Divider(
                thickness: 1.0,
                endIndent: 100.0,
                indent: 100.0,
                color: Colors.grey,
              ),
              driver(),
              textfiled(),
              sendrequest()
            ],
          ),
        );
      },
    );
  }

  driver() {
    return InkWell(
      splashColor: Colors.amber,
      onTap: () {
        setState(() {
          print(namedriver);
        });
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.75,
        height: 120,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: namedriver.length,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                width: 110,
                height: 110,
                decoration: decdriver(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ClipOval(
                      child: Image.asset(imgdriver[index]),
                    ),
                    Text(namedriver[index]),
                    Text('58679')
                  ],
                ),
              );
            }),
      ),
    );
  }

  sendrequest() {
    return InkWell(
      onTap: () {
        setState(() {});
      },
      child: Container(
        margin: EdgeInsets.only(right: 20, left: 20, top: 15),
        decoration: validityorgin(),
        width: MediaQuery.of(context).size.width * 1,
        height: 50,
        child: Center(
          child: Text(
            'ارسال درخواست',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
        ),
      ),
    );
  }

  textfiled() {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 50,
            child: TextField(
              controller: txttime,
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.more_time_rounded),
                prefixIcon: IconButton(
                    splashColor: Colors.white,
                    onPressed: () {
                      print('object');
                    },
                    icon: Icon(Icons.keyboard_arrow_down)),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'وقف در مسیر دارید؟',
                fillColor: Colors.blue[100],
                filled: true,
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 50,
            child: TextField(
              controller: txtdiscount,
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.price_change_sharp),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'کد تخفیف دارید؟',
                fillColor: Colors.blue[100],
                filled: true,
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                  flex: 2,
                  child: SizedBox(
                    width: 180,
                    height: 50,
                    child: TextField(
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        prefixIcon: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.keyboard_arrow_down)),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20)),
                        hintText: 'پرداخت از اعتبار',
                        fillColor: Colors.blue[100],
                        filled: true,
                      ),
                    ),
                  )),
              Flexible(
                flex: 2,
                child: SizedBox(
                  width: 180,
                  height: 50,
                  child: TextField(
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                      hintText: 'هزینه',
                      fillColor: Colors.blue[100],
                      filled: true,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
