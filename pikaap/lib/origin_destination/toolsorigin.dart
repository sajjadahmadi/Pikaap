import 'package:flutter/material.dart';

TextEditingController txtorigin = TextEditingController();
TextEditingController txtdestina = TextEditingController();
TextEditingController txttime = TextEditingController();
TextEditingController txtdiscount = TextEditingController();
decbottomsheet() {
  return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)));
}

validityorgin() {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        Color(0xff4f5d7e),
        Color(0xff132141),
      ],
    ),
    borderRadius: BorderRadius.circular(15),
  );
}

decdriver() {
  return BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    border: Border.all(width: 1, color: Colors.grey),
  );
}

List<String> imgdriver = ['images/d1.png', 'images/d2.png', 'images/d3.png'];
List<String> namedriver = ['فیروز کریمی', 'امیرمحمد نوری', 'محمد امینی'];
