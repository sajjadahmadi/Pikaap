import 'package:flutter/material.dart';
import 'package:pikaap/origin_destination/destination.dart';
import 'package:pikaap/origin_destination/toolsorigin.dart';
import 'package:pikaap/home.dart';
import '../toolshome.dart';

class Origin extends StatefulWidget {
  @override
  _OriginState createState() => _OriginState();
}

class _OriginState extends State<Origin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color1,
      body: Stack(
        // fit: StackFit.expand,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/maps.png'), fit: BoxFit.cover)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Home();
                        }));
                      });
                    },
                    icon: Icon(Icons.keyboard_arrow_right))
              ],
            ),
          ),
          DraggableScrollableSheet(
            initialChildSize: 0.1,
            minChildSize: 0.1,
            maxChildSize: 0.3,
            builder: (context, controller) {
              return Container(
                height: 50,
                decoration: decbottomsheet(),
                child: ListView(
                  controller: controller,
                  children: [
                    Divider(
                      thickness: 1.0,
                      endIndent: 100.0,
                      indent: 100.0,
                      color: Colors.grey,
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.10,
                        height: 50,
                        child: TextField(
                          controller: txtorigin,
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.add_location_alt_rounded),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            hintText: 'مبداتان کجاست؟',
                            fillColor: Colors.blue[100],
                            filled: true,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Destination();
                          }));
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 20, left: 20, top: 15),
                        decoration: validityorgin(),
                        width: MediaQuery.of(context).size.width * 1,
                        height: 50,
                        child: Center(
                          child: Text(
                            'تایید مبدا',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
