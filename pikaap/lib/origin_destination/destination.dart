import 'package:flutter/material.dart';
import 'package:pikaap/origin_destination/details.dart';
import 'package:pikaap/origin_destination/toolsorigin.dart';
import 'package:pikaap/toolshome.dart';
import 'package:pikaap/home.dart';

class Destination extends StatefulWidget {
  @override
  _DestinationState createState() => _DestinationState();
}

class _DestinationState extends State<Destination> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color1,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/maps.png'), fit: BoxFit.cover)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Home();
                        }));
                      });
                    },
                    icon: Icon(Icons.keyboard_arrow_right))
              ],
            ),
          ),
          DraggableScrollableSheet(
            initialChildSize: 0.1,
            minChildSize: 0.1,
            maxChildSize: 0.3,
            builder: (context, controller) {
              return Container(
                height: 50,
                decoration: decbottomsheet(),
                child: ListView(
                  controller: controller,
                  children: [
                    Divider(
                      thickness: 1.0,
                      endIndent: 100.0,
                      indent: 100.0,
                      color: Colors.grey,
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.10,
                        height: 50,
                        child: TextField(
                          controller: txtdestina,
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.flag),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            hintText: 'مقصدتان کجاست؟',
                            fillColor: Colors.blue[100],
                            filled: true,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Details();
                          }));
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 20, left: 20, top: 15),
                        decoration: validityorgin(),
                        width: MediaQuery.of(context).size.width * 1,
                        height: 50,
                        child: Center(
                          child: Text(
                            'تایید مقصد',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
