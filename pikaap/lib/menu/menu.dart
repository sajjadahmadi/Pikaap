import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pikaap/toolshome.dart';
import 'toolsmenu.dart';
import 'package:get/get.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              CircleAvatar(
                backgroundColor: color1,
                child: IconButton(
                    onPressed: () {
                      setState(() {
                        Get.back();
                      });
                    },
                    icon: Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.black,
                    )),
              ),
              Expanded(
                  child: Container(
                child: ListView.builder(
                    itemCount: namemenu.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        splashColor: Colors.purple,
                        onTap: () {
                          setState(() {});
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          height: 40,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Icon(Icons.keyboard_arrow_left),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    namemenu[index],
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.grey),
                                  ),
                                  Image.asset(
                                    Imagemenu[index],
                                    width: 25,
                                    height: 25,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )),
            ],
          ),
        ),
      ),
    );
  }
}
