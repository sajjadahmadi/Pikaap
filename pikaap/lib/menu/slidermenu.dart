import 'package:flutter/material.dart';
import 'menu.dart';

class SliderMenu extends PageRouteBuilder {
  final Widget widget;

  SliderMenu(this.widget)
      : super(
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return SlideTransition(
                position: Tween(
                  begin: Offset(0.5, -0.5),
                  end: Offset.zero,
                ).animate(animation),
                child: child,
              );
            },
            transitionDuration: Duration(seconds: 2),
            pageBuilder: (context, animation, secondaryAnimation) {
              return Menu();
            });
}
