import 'package:flutter/material.dart';

List<String> nameitem = [
  'پیکاپ ',
  'پیکاپ باکس',
  'پیکاپ پست',
  'پیکاپ موتور',
];
List<Image> imggride = [
  Image.asset(
    'images/home/box.png',
    width: 80,
    height: 80,
  ),
  Image.asset(
    'images/home/pikap.png',
    width: 80,
    height: 80,
  ),
  Image.asset(
    'images/home/post.png',
    width: 80,
    height: 80,
  ),
  Image.asset(
    'images/home/motor.png',
    width: 80,
    height: 80,
  ),
];
List<Color> begincolor = [
  Color(0xffa34bfa),
  Color(0xff395ff8),
  Color(0xffff3838),
  Color(0xfff8b93a),
];

List<Color> endcolor = [
  Color(0xffc789f6),
  Color(0xff7893ff),
  Color(0xfff57578),
  Color(0xffffd475),
];
decorationgrid() {
  return BoxDecoration(
      borderRadius: BorderRadius.circular(10), color: Colors.blue);
}

String strtabligh = 'تبلیغات شما با ما';
TextStyle styletabligh =
    TextStyle(fontSize: 35, fontWeight: FontWeight.normal, color: Colors.white);
Color color2 = Color(0xfff4f7ff);
Color color1 = Color(0xffBFCDF3);
decrotionitem1() {
  return BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(20),
  );
}

decrotiontabligh() {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        Color(0xff6c88c7),
        Color(0xff03194c),
      ],
    ),
    borderRadius: BorderRadius.circular(20),
  );
}

TextStyle styletxtgrid = TextStyle(fontSize: 15, color: Colors.white);
decrotioncardtitle() {
  return BoxDecoration(
      borderRadius: BorderRadius.circular(15), color: Colors.white);
}
