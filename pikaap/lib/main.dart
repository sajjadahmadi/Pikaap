import 'package:flutter/material.dart';
import 'package:pikaap/home.dart';
import 'package:pikaap/origin_destination/destination.dart';
import 'package:pikaap/origin_destination/details.dart';
import 'package:pikaap/origin_destination/origin.dart';
import 'package:pikaap/wallet/wallet.dart';
import 'toolshome.dart';
import 'package:get/get.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(GetMaterialApp(
    title: 'Pikaap',
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}
